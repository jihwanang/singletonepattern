using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPool : Singleton<ObjectPool>
{
    //PooledObject 클래스를 리스트로 관리하는 변수
    public List<PooledObject> objectPool = new List<PooledObject>();

    private void Awake()
    {
        // 오브젝트 풀을 초기화 한다.
        for (int ix = 0; ix < objectPool.Count; ++ix)
        {
            objectPool[ix].Initialize(this.transform);
        }
    }

    PooledObject GetPoolItem (string itemName)
    {
        /*
         * GetpoolItem은 전달된 itemName 파라미터와 같은 이름을 가진 오브젝트 풀을 검색하고,
         * 검색에 성공하면 그 결과 값을 반환한다.
         */
        for (int ix = 0; ix < objectPool.Count; ++ix)
        {
            if (objectPool[ix].poolItemName.Equals(itemName))
                return objectPool[ix];
        }
        Debug.LogWarning("There's no matched pool list");
        return null;
    }

    public bool PushToPool(string itemName,          /*반환할 객체의 pool 오브젝트 이름*/
                           GameObject item,          /*반환할 객체(게임오브젝트)*/
                           Transform parent = null)  /*부모 계층 관계를 설정할 정보*/
    {
        /*
         * 사용한 객체를 ObjectPool에 반환한다.
         */
        PooledObject pool = GetPoolItem(itemName);
        if (pool == null)
            return false;

        // 삼항연산자
        pool.PushToPool(item, parent == null ? transform : parent);
        return true;
    }

    public GameObject PopFromPool (string itemName,/*요청할 객체의 pool 오브젝트의 이름*/
                                   Transform parent = null)
    {
        // 필요한 객체를 오브젝트 풀에 요청할때 사용
        PooledObject pool = GetPoolItem(itemName);
        if (pool == null)
            return null;

        return pool.PopFromPool(parent);
    }
}
