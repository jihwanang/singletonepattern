using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PooledObject
{
    public string poolItemName = string.Empty;   // 객체를 검색할 때 사용할 이름
    public GameObject prefab = null;             // 오브젝트 풀에 저장할 프리펩
    public int poolCount = 0;                    //초기화할 때 생성할 객체의 수
    [SerializeField]
    private List<GameObject> poolList = new List<GameObject>(); // 생성한 객체들을 저장할 리스트

    public void Initialize(Transform parent/*생성된 객체들을 정리하는 용도로 사용*/ = null)
    {
        // PoolObject 객체를 초기화 할때 처음 한번만 호출된다.
        for (int ix = 0; ix < poolCount; ++ix)
        {
            poolList.Add(CreateItem(parent));
        }
    }

    private GameObject CreateItem(Transform parent = null)
    {
        /*
         * prefab 변수에 지정된 게임 오브젝트를 생성한다.
         */
        GameObject item = Object.Instantiate(prefab) as GameObject;
        item.name = poolItemName;
        item.transform.SetParent(parent);
        item.SetActive(false);
        return item;
    }

    public GameObject PopFromPool(Transform parent = null)
    {
        /*
         * 객체가 필요할 때 오브젝트 풀에 요청한다
         */
        if (poolList.Count == 0) // 저장해둔 오브젝트가 남아 있는지 확인하고, 없으면 새로 생성해서 추가한다.
            poolList.Add(CreateItem(parent));
        GameObject item = poolList[0]; // 미리 저장해둔 리스트에서 하나 꺼내고 이 객체를 반환한다.
        poolList.RemoveAt(0);
        return item;
    }

    public void PushToPool(GameObject item, Transform parent = null)
    {
        /*
         * 사용한 객체를 다시 오브젝트 풀에 반환한다
         */
        item.transform.SetParent(parent);
        item.SetActive(false);
        poolList.Add(item);
    }
}

