using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletMove : MonoBehaviour
{
    public string poolItemName = "Bullet";  // 오브젝트 풀에 저장된 bullet 오브젝트 이름
    public float moveSpeed = 10.0f;         // 총알의 이동속도
    public float lifeTime = 3.0f;           // 총알의 수명
    private float _elapsedTime = 0f;        // 총알이 활성화된 뒤 경과시간을 계산하기 위한 변수

    private void Update()
    {
        transform.position += transform.up * moveSpeed * Time.deltaTime;

        if (GetTimer() > lifeTime) // 누적된(현재) 시간이 경과된 시간을 확인한다.
        {
            SetTimer();
            ObjectPool.Instance.PushToPool(poolItemName, gameObject);
        }
    }

    float GetTimer()
    {
        // 경과된 시간을 누적한다 : 현재시간
        return (_elapsedTime += Time.deltaTime);
    }

    void SetTimer()
    {
        // 타이머를 초기화 한다.
        _elapsedTime = 0f;
    }
}
