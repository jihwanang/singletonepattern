using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public string bulletName = "Bullet";

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Shoot();
        }
    }

    void Shoot()
    {
        // 오브젝트 풀로 부터 총알 객체를 요청한다
        GameObject bullet = ObjectPool.Instance.PopFromPool(bulletName);
        bullet.transform.position = transform.position + transform.up;
        bullet.SetActive(true);
    }
}
