using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ObjectPooling.Multi
{
    /// <summary>
    /// 이 클래스는 오브젝트 풀의 기본 속성을 정의한다.
    /// 현재는 오브젝트 풀에서 사용할 프리팹과 크기를 저장한다.
    /// </summary>
    [System.Serializable]
    public class ObjectPool
    {
        public GameObject prefab;
        public int size;
    }

    public class PoolManagerMulti : MonoBehaviour
    {
        public List<ObjectPool> ObjectPoolList;

        private Dictionary<string, Queue<GameObject>> objectPoolDictinary;

        #region SingleTone
        private static PoolManagerMulti _instance = null;

        public static PoolManagerMulti Instance
        {
            get
            {
                if(_instance == null)
                {
                    _instance = (PoolManagerMulti)FindObjectOfType(typeof(PoolManagerMulti));
                    if(_instance == null)
                    {
                        Debug.Log("There's no active Singletone object");
                    }
                }
                return _instance;
            }
        }

        private void Awake()
        {
            if(_instance != null && _instance != this)
            {
                DestroyImmediate(gameObject);
            }
            else
            {
                _instance = this;
                Init();
            }
        }
        #endregion

        GameObject CreateGameObject(GameObject prefab)
        {
            GameObject obj  = GameObject.Instantiate(prefab);
            obj.name = prefab.name;
            obj.SetActive(false);
            obj.transform.parent = this.transform;
            return obj;
        }

        ObjectPool FindObjectPool(string poolName)
        {
            return ObjectPoolList.Find(x => (x.prefab != null && string.Equals(x.prefab.name, poolName)));
        }
        /// <summary>
        /// 오브젝트 풀을 초기화 한다. 여기서 오브젝트가 인스턴스화 되고 각각의 오브젝트를 풀에 저장한다.
        /// </summary>
        private void Init()
        {
            objectPoolDictinary = new Dictionary<string, Queue<GameObject>>();
            foreach(var pool in ObjectPoolList)
            {
                if(!pool.prefab)
                {
                    Debug.LogError("Invalid prefab");
                    continue;
                }

                // 오브젝트 풀을 만들고 여기에 오브젝트를 저장한다.
                Queue<GameObject> poolQueue = new Queue<GameObject>();
                for(int i = 0; i < pool.size; i++)
                {
                    poolQueue.Enqueue(CreateGameObject(pool.prefab));
                }

                //Dictionary 에 오브젝트 풀 추가
                objectPoolDictinary.Add(pool.prefab.name, poolQueue);
            }
        }


        /// <summary>
        /// 오브젝트 풀에서 오브젝트 가져오기
        /// 오브젝트의 위치와 회전을 설정한다
        /// </summary>
        public GameObject Spawn (string poolName, Vector3 position, Quaternion rotation)
        {
            GameObject obj = null;

            if(objectPoolDictinary.ContainsKey(poolName))
            {
                Queue<GameObject> poolQueue = objectPoolDictinary[poolName];

                if(poolQueue.Count > 0)
                {
                    //오브젝트 풀이 있을때 오브젝트를 반환한다.
                    obj = poolQueue.Dequeue();
                }
                else
                {
                    //오브젝트 풀이 비어있다.
                    //Debug.Log(poolName + "dobject pool is empty");
                    ObjectPool pool = FindObjectPool(poolName);
                    if(pool != null)
                    {
                        obj = CreateGameObject(pool.prefab);
                    }
                }

                if(obj != null)
                {
                    obj.transform.position = position;
                    obj.transform.rotation = rotation;
                    obj.SetActive(true);
                }
            }
            else
            {
                Debug.LogError(poolName + "object pool is not available");
            }
            return obj;
        }

        /// <summary>
        /// 오브젝트 풀에 오브젝트 반환
        /// </summary>
        /// <returns></returns>
        private IEnumerator _despawn(GameObject poolObject, float timer)
        {
            yield return new WaitForSeconds(timer);

            if(objectPoolDictinary.ContainsKey(poolObject.name))
            {
                Queue<GameObject> poolQueue = objectPoolDictinary[poolObject.name];

                if(poolQueue.Contains(poolObject) == false) // 오브젝트 풀에 이미 등록 되어 있는지 검사
                {
                    objectPoolDictinary[poolObject.name].Enqueue(poolObject);
                    poolObject.SetActive(false);
                }
            }
            else
            {
                // 이미 처리됨
                Debug.LogError(poolObject.name + "object pool is not available");
            }
        }

        public void Despawn(GameObject poolObject, float timer = 0f)
        {
            StartCoroutine(_despawn(poolObject, timer));
        }
        
    }
}
