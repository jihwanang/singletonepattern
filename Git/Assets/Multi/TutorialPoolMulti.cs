using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ObjectPooling.Multi
{
    public class TutorialPoolMulti : MonoBehaviour
    {
        Vector3 spawnPosition;

        private void Start()
        {
            StartCoroutine(SpawnObjects());
        }

        // 일정시간 간격으로 오브젝트 풀에서 오브젝트 가져오기
        private IEnumerator SpawnObjects()
        {
            while(true)
            {
                GameObject go;
                float xValue = Random.Range(-5f, 5f);
                spawnPosition = new Vector3(xValue, 2f, -0.5f);

                go = PoolManagerMulti.Instance.Spawn("Cube", spawnPosition, Quaternion.identity);
                if(go != null)
                {
                    ApplyForce(go.transform);
                    PoolManagerMulti.Instance.Despawn(go, 2f);
                }

                yield return new WaitForSeconds(0.1f);
                xValue = Random.Range(-5f, 5f);
                go = PoolManagerMulti.Instance.Spawn("Sphere", spawnPosition, Quaternion.identity);
                if( go != null)
                {
                    ApplyForce(go.transform);
                    PoolManagerMulti.Instance.Despawn(go, 2f);
                }

                yield return new WaitForSeconds(0.1f);
            }
        }

        public void ApplyForce(Transform target)
        {
            Rigidbody rb = target.GetComponent<Rigidbody>();
            if (!rb) return;

            rb.velocity = Vector3.zero;
            rb.AddForce(transform.up * 200.0f);
            rb.useGravity = true;
        }
    }
}
