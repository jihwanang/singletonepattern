using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateMachine<T>
{
    public IState<T> CurState { get; protected set; }

    private T m_sender;

    public StateMachine(T sender, IState<T> state)
    {
        m_sender = sender;
        SetState(state);
    }

    public void SetState(IState<T> state)
    {
        if (m_sender == null)
        {
            Debug.LogError("invalid m_sender");
            return;
        }

        if(CurState == state)
        {
            Debug.LogWarningFormat("Already Define State - {0}", state);
            return;
        }

        if (CurState != null)
            CurState.OnExit(m_sender); // 새로운 포인트이동 Entry

        CurState = state;

        if (CurState != null)
            CurState.OnEnter(m_sender);
    }

    public void OnFixedUpdate()
    {
        if(m_sender == null)
        {
            Debug.LogError("invalid m_sender");
            return;
        }
        CurState.OnFixedUpdate(m_sender); // 물리적인 업데이트
    }

    public void OnUpdate()
    {
        if(m_sender == null)
        {
            Debug.LogError("invalid m_sender");
            return;
        }
        CurState.OnUpdate(m_sender);
    }
}

