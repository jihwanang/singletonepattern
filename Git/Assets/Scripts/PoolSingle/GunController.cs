using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ObjectPooling.Single
{
    public class GunController : MonoBehaviour
    {
        public BulletObjectPool bulletPool;

        private float rotSpeed = 60.0f;
        private float fireTimer;
        private float fireInterval = 0.1f;

        void Start()
        {
            fireTimer = Mathf.Infinity; // 무한대의 숫자 1을 0으로 나눈다    

            if(bulletPool == null)
            {
                Debug.LogError("Need a reference to the object pool ");
            }
        }

        void Update()
        {
            // Rotate Gun
            if (Input.GetKey(KeyCode.A))
            {
                this.transform.Rotate(Vector3.up, -rotSpeed * Time.deltaTime);
            }
            else if (Input.GetKey(KeyCode.D))
            {
                this.transform.Rotate(Vector3.up, rotSpeed * Time.deltaTime);
            }

            // Fire Gun
            if (Input.GetKey(KeyCode.Space) && fireTimer > fireInterval)
            {
                fireTimer = 0f;

                GameObject newBullet = GetABullet();
                
                if(newBullet != null)
                {
                    newBullet.SetActive(true);
                    newBullet.transform.forward = transform.forward;
                    newBullet.transform.position = transform.position + transform.forward * 2;
                }
                else
                {
                    Debug.Log("Couldn't find a new bullet");
                }
            }

            fireTimer += Time.deltaTime;
        }

        private GameObject GetABullet()
        {
            GameObject bullet = bulletPool.GetBullet();
            return bullet;
        }
    }
}