using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveBullet : MonoBehaviour
{
    private float bulletSpeed = 10f;
    private float deactivationDistance = 15f;

    private void Update()
    {
        transform.Translate(Vector3.forward * bulletSpeed * Time.deltaTime);

        if(Vector3.SqrMagnitude(transform.position) > deactivationDistance * deactivationDistance)
        {
            gameObject.SetActive(false);
        }
    }
}
