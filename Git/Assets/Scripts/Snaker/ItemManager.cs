using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemManager : MonoBehaviour
{
    public GameObject box;

    public IEnumerator ReSpawnItem(float delay)
    {
        box.SetActive(false);
        yield return new WaitForSeconds(delay);
        box.SetActive(true);
        box.transform.localPosition = new Vector3(Random.Range(-4f, 4f), 0.5f, Random.Range(-4f, 4f));
    }

}
