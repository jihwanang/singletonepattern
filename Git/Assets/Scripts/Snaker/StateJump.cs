using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateJump : IState<SnakerCtrl>
{
    public void OnEnter(SnakerCtrl snaker)
    {
        Rigidbody _rigid = snaker.GetComponent<Rigidbody>();
        _rigid.AddForce(Vector3.up * snaker.jump, ForceMode.Impulse);
    }

    public void OnExit(SnakerCtrl snaker)
    {

    }

    public void OnFixedUpdate(SnakerCtrl snaker)
    {
        snaker.DoMove();
    }

    public void OnUpdate(SnakerCtrl snaker)
    {

    }
}
