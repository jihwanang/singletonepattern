using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateDead : IState<SnakerCtrl>
{
    public void OnEnter(SnakerCtrl snaker)
    {
        snaker.GetComponent<Rigidbody>().isKinematic = true;
    }

    public void OnExit(SnakerCtrl snaker)
    {

    }

    public void OnFixedUpdate(SnakerCtrl snaker)
    {

    }

    public void OnUpdate(SnakerCtrl snaker)
    {
        if (Input.GetKeyDown(KeyCode.Space))
            snaker.ChangState(SnakerCtrl.eState.Ready);
    }
}
