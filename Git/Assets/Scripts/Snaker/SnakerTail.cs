using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public class SnakerTail : MonoBehaviour
{
    private Transform m_target;
    private Transform _tr;
    public float offset_z = 3f;
    public float moveSpeed = 3f;
    public float rotSpeed = 3f;
    public bool Pause { get; set; } = false;

    private void Awake()
    {
        m_target = null;
        _tr = this.transform.GetComponent<Transform>();
    }

    public void SetTarget(Transform target)
    {
        this.m_target = target;
        this.m_target.position = target.position;
    }

    private void Update()
    {
        if (!m_target)
            return;
        if (Pause)
            return;

        /*
         * ���ʹϾ� * ���ʹϾ� = ���� ���Ѵ�
         * Q * Q = Q
         * 
         * ���ʹϾ� * ���� = ���͸� ȸ���Ѵ�.(���ʹϾ��� ��������)
         * Q * Vector3.forward = V
         * 
         * camera.traosfrom.forward
         */
        Vector3 target_pos = m_target.position - (m_target.forward * offset_z);
        _tr.position = Vector3.Lerp(_tr.position, target_pos, moveSpeed * Time.deltaTime);

        Quaternion lookAt = Quaternion.LookRotation(target_pos - transform.position);
        transform.rotation = Quaternion.Slerp(transform.rotation, lookAt, rotSpeed * Time.deltaTime);
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawRay(this.transform.position, this.transform.forward * 0.5f);
    }
}
