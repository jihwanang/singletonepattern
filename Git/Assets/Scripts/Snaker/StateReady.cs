using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateReady : IState<SnakerCtrl>
{
    Rigidbody _rigid;

    public void OnEnter(SnakerCtrl snaker)
    {
        _rigid = snaker.GetComponent<Rigidbody>();
        _rigid.isKinematic = true;

        snaker.transform.position = snaker.Start_Position;
        snaker.transform.rotation = Quaternion.Euler(snaker.Start_Rotation);
    }

    public void OnExit(SnakerCtrl snaker)
    {

    }

    public void OnFixedUpdate(SnakerCtrl snaker)
    {

    }

    public void OnUpdate(SnakerCtrl snaker)
    {
        if (Input.GetKeyDown(KeyCode.Space))
            _rigid.isKinematic = false;
    }
}
