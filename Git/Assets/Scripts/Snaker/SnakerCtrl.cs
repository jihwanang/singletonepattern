using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class SnakerCtrl : MonoBehaviour
{
    public enum eState
    {
        Ready,
        Move,
        Jump,
        Dead,
    }

    private StateMachine<SnakerCtrl> m_sm;
    private Dictionary<eState, IState<SnakerCtrl>> m_states = new Dictionary<eState, IState<SnakerCtrl>>();

    public float input_horizontal { get; set; }

    public TailManager m_tailManager;
    public ItemManager m_itemManger;

    public float moveSpeed = 10.0f;
    public float accelerate = 0f;
    public float rotSpeed = 90.0f;
    public float jump = 3.0f;
    public bool UsingAuto = true;

    public Vector3 Start_Position { get; set; }
    public Vector3 Start_Rotation { get; set; }

    private Rigidbody _rigid;

    SnakerTail last_tail = null;

    private void Awake()
    {
        Start_Position = this.transform.position;
        Start_Rotation = this.transform.eulerAngles;
    }

    void Start()
    {
        m_states.Add(eState.Ready, new StateReady());
        m_states.Add(eState.Move, new StateMove());
        m_states.Add(eState.Jump, new StateJump());
        m_states.Add(eState.Dead, new StateDead());
        m_sm = new StateMachine<SnakerCtrl>(this, m_states[eState.Ready]);

        _rigid = GetComponent<Rigidbody>();
    }

    public void ChangState(eState state)
    {
        m_sm.SetState(m_states[state]);
    }

    private void FixedUpdate()
    {
        m_sm.OnFixedUpdate();
    }

    void KeybowardInput()
    {
        input_horizontal = Input.GetAxis("Horizontal");
    }

    private void Update()
    {
        KeybowardInput();

        m_sm.OnUpdate();
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawRay(this.transform.position, this.transform.forward * 1f);

    }

    private void OnTriggerEnter(Collider other)
    {
        if(true == other.name.Contains("ItemBox"))
        {
            StartCoroutine(m_itemManger.ReSpawnItem(0.4f));
            accelerate = 1.4f;
        } else if(true == other.name.Contains("DeadZone")) {
            m_sm.SetState(m_states[eState.Dead]);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.CompareTag("Ground"))
        {
            ChangState(eState.Move);
        }
    }

    public void DoMove()
    {
        accelerate = Mathf.Lerp(accelerate, 0, Time.deltaTime);
        _rigid.MovePosition(_rigid.position + (transform.forward * (moveSpeed + accelerate) * Time.deltaTime));
    }

    public void DoRotation()
    {

        if (UsingAuto == true)
        {
            Quaternion lookAt = Quaternion.LookRotation(m_itemManger.box.transform.position - this.transform.position);
            _rigid.rotation = Quaternion.Slerp(_rigid.rotation, lookAt, rotSpeed * Time.deltaTime);

        }
        else {
            _rigid.rotation *= Quaternion.Euler(0f, input_horizontal * rotSpeed * Time.deltaTime, 0f);
        }
    }

    public void AddTail()
    {
        Transform target = this.transform;
        if (last_tail != null)
            target = last_tail.transform;

        last_tail = m_tailManager.CreateTail(target);

        StartCoroutine(m_itemManger.ReSpawnItem(0f));

        accelerate = 2f;
    }
}
