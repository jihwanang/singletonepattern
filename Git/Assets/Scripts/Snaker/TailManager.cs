using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TailManager : MonoBehaviour
{
    public GameObject prefabTail;

    public SnakerTail CreateTail(Transform target)
    {
        if (!prefabTail || !target)
            return null;

        GameObject go = GameObject.Instantiate(prefabTail) as GameObject;
        SnakerTail tail = go.GetComponent<SnakerTail>();
        if(!tail)
        {
            Debug.LogError("Not Found SnakerTail Componenet");
            return null;
        }

        tail.SetTarget(target.transform);
        go.transform.position = target.position;
        go.transform.rotation = Quaternion.identity;
        go.transform.parent = this.transform;

        return tail;
    }

    public void PauseAll()
    {
        SnakerTail[] tails = GetComponentsInChildren<SnakerTail>(true);

        foreach (var tail in tails)
        {
            tail.Pause = true;
        }
    }

    public void ClaerAll()
    {
        SnakerTail[] tails = GetComponentsInChildren<SnakerTail>(true);

        foreach (var tail in tails)
        {
            Destroy(tail.gameObject);
        }
    }
}
