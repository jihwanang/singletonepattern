using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateMove : IState<SnakerCtrl>
{
    public void OnEnter(SnakerCtrl snaker)
    {
        snaker.GetComponent<Rigidbody>().isKinematic = false;
    }

    public void OnExit(SnakerCtrl snaker)
    {

    }

    public void OnFixedUpdate(SnakerCtrl snaker)
    {
        snaker.DoMove();
        snaker.DoRotation();
    }

    public void OnUpdate(SnakerCtrl snaker)
    {
        if (Input.GetKeyDown(KeyCode.Space))
            snaker.ChangState(SnakerCtrl.eState.Jump);
    }
}
