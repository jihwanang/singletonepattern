using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossMovePattern : MonoBehaviour
{
    public string[] levels;

    private List<string> ParseCommands(string str)
    {
        List<string> list = new List<string>();
        string[] splits = str.Split(',');
        foreach (var split in splits)
            list.Add(split);

        return list;
    }

    public void DoNext(BossCtrl boss)
    {
        if (levels.Length == 0)
            return;

        int id = Random.Range(0, levels.Length);
        List<string> commands = ParseCommands(levels[id]);
        StartCoroutine(DoCommands(boss, commands));
    }

    IEnumerator DoCommands(BossCtrl boss, List<string> commands)
    {
        int idx = 0;
        while (idx <= commands.Count -1)
        {
            ChangsState(boss, commands[idx++]);
            yield return new WaitForSeconds(1.0f);
        }
        boss.ChangeState(BossCtrl.eState.Stop);
    }

    void ChangsState(BossCtrl boss, string command)
    {
        switch(command)
        {
            case "L":
                boss.ChangeState(BossCtrl.eState.Left);
                break;            

            case "R":
                boss.ChangeState(BossCtrl.eState.Right);
                break;     
                
            case "U":
                boss.ChangeState(BossCtrl.eState.Up);
                break;  
                
            case "D":
                boss.ChangeState(BossCtrl.eState.Down);
                break;
        }
    }
}
