using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class BossCtrl : MonoBehaviour
{
    public enum eState
    {
        Stop, Left, Right, Up, Down
    }

    private StateMachine<BossCtrl> m_sm;
    private Dictionary<eState, IState<BossCtrl>> m_state = new Dictionary<eState, IState<BossCtrl>>();

    public float moveSpeed = 5.0f;

    public Vector3 Start_Position { get; set; }
    public Vector3 Start_Rotation { get; set; }

    private Rigidbody _rigid;

    private BossMovePattern pattern;

    private void Awake()
    {
        pattern = this.transform.GetComponent<BossMovePattern>();
        Start_Position = this.transform.position;
        Start_Rotation = this.transform.eulerAngles;
    }

    void Start()
    {
        m_state.Add(eState.Stop,    new BossStateStop()); // 인터페이스 생성(호출)
        m_state.Add(eState.Left,    new BossStateLeft());
        m_state.Add(eState.Right,   new BossStateRight());
        m_state.Add(eState.Up,      new BossStateUp());
        m_state.Add(eState.Down,    new BossStateDown());

        m_sm = new StateMachine<BossCtrl>(this, m_state[eState.Down]);

        _rigid = GetComponent<Rigidbody>();
    }

    public void ChangeState (eState state)
    {
        Debug.LogWarning(state);
        m_sm.SetState(m_state[state]); // 실제 state 가져오는곳
    }

    private void FixedUpdate()
    {
        m_sm.OnFixedUpdate();
    }

    void Update()
    {
        m_sm.OnUpdate();

        if(m_sm.CurState == m_state[eState.Stop])
        {
            pattern.DoNext(this);
        }
    }

    public void DoMove(Vector3 dir)
    {
        _rigid.MovePosition(_rigid.position + (dir * moveSpeed * Time.deltaTime));
    }
}
