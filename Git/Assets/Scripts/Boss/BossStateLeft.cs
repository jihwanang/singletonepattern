using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossStateLeft : IState<BossCtrl>
{
    public void OnEnter(BossCtrl boss)
    {

    }

    public void OnExit(BossCtrl boss)
    {

    }

    public void OnFixedUpdate(BossCtrl boss)
    {

    }

    public void OnUpdate(BossCtrl boss)
    {
        boss.DoMove(Vector3.left);
    }
}
