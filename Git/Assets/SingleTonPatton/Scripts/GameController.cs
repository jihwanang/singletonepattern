using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SingletonPattern
{
    public class GameController : MonoBehaviour
    {
        public void TestCharpSingleton()
        {
            Debug.Log("C#");

            SingleTonCSharp instance = SingleTonCSharp.Instance;
            instance.TestSingleton();

            SingleTonCSharp instanc2 = SingleTonCSharp.Instance;
            instanc2.TestSingleton();
        }

        public void TestUnitySingleton()
        {
            Debug.Log("Unity");

            SingleTonUnity instance = SingleTonUnity.Instacne;
            instance.TestSingleton();
            
            SingleTonUnity instance2 = SingleTonUnity.Instacne;
            instance2.TestSingleton();


        }
    }
}