using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SingletonPattern
{
    public class SingleTonUnity : MonoBehaviour
    {
        private static SingleTonUnity instance = null;

        public static SingleTonUnity Instacne
        {
            get
            {
                if(instance == null)
                {
                    SingleTonUnity[] allsingletonsInScens = GameObject.FindObjectsOfType<SingleTonUnity>();

                    if (allsingletonsInScens != null && allsingletonsInScens.Length > 0)
                    {
                        if (allsingletonsInScens.Length > 1)
                        {
                            Debug.LogWarning("Already Define SingleTone in the scene!");

                            for (int i = 1; i < allsingletonsInScens.Length; i++)
                            {
                                Destroy(allsingletonsInScens[i].gameObject);
                            }
                        }

                        instance = allsingletonsInScens[0];

                        instance = allsingletonsInScens[0];

                        //�ʱ�ȭ
                        instance.FakeConstructor();
                    }
                    else
                    {
                        Debug.LogError($"Not Define Singtone Object");
                    }
                }

                return instance;
            }
        }
        private float randomNumber;

        private void FakeConstructor()
        {
            randomNumber = Random.Range(0f, 1f);
        }

        public void TestSingleton()
        {
            Debug.Log($"Hello Singleton, random number is : {randomNumber}");
        }
    }
}
