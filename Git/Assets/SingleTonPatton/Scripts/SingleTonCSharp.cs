using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SingletonPattern
{
    public class SingleTonCSharp
    {
        private static SingleTonCSharp instance = null;

        public static SingleTonCSharp Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new SingleTonCSharp();
                }

                return instance;
            }
        }
        private float randomNumber;

        private SingleTonCSharp()
        {
            randomNumber = Random.Range(0f, 1f);
        }

        public void TestSingleton()
        {
            Debug.Log($"Hello Singleton, random number is : {randomNumber}");
        }
    }
}
